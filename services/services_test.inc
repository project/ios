<?php

/**
 * Callback for 'services'.
 */
function services_test_index() {
  $methods = services_get_all();
  
  // Show enable server modules
  $servers = module_implements('server_info');
  $output = '<h2>' . t('Servers') . '</h2>';
  
  if (!empty($servers)) {
    $output .= '<ul>';
    foreach ($servers as $module) {
      $info = module_invoke($module, 'server_info');
      $name = $info['#name'];
      $path = 'services/'. $info['#path'];
      $output .= '<li class="leaf">'. l($name. " - /$path", $path) .'</li>';
    }
    $output .= '</ul>';
  }
  else {
    $output .= '<p>'. t('You must enable at least one server module to be able to connect remotely. Visit the <a href="@url">modules</a> page to enable server modules.', array('@url' => url('admin/structure/modules'))) .'</p>';
  }
  
  
  $output .= '<h2>'. t('Services') .'</h2>';
  
  // group namespaces
  $services = array();
  foreach ($methods as $method) {
    $namespace = substr($method['#method'], 0, strrpos($method['#method'], '.'));
    $services[$namespace][] = $method;
  }
  
  if (count($services)) {
    foreach ($services as $namespace => $methods) {
      $output .= '<h3>'. $namespace .'</h3>';
      $output .= '<ul>';
      foreach ($methods as $method) {
        $output .= '<li class="leaf">'. l($method['#method'], 'admin/structure/services/'. $method['#method']) .'</li>';
      }
      $output .= '</ul>';
    }
  }
  else {
    $output .= t('No services have been enabled.');
  }
  
  return $output;
}


/**
 * Allow users to test a given service.
 *
 * @param $method
 *   Object. The service info.
 *
 * @ingroup form
 */
function _services_test($form_state, $service) {
  // Testing fields
  $form['arg'] = array(
    '#tree'   => TRUE,
  );
  foreach ($service['#args'] as $key => $arg) {
    $form['arg'][$key] = array(
      '#description'  => $arg['#description'],
      '#required'     => !$arg['#optional'],
      '#title'        => $arg['#name'],
      '#type'         => 'textfield',
    );
    if (isset($arg['#size']) and $arg['#size'] == 'big') {
      $form['arg'][$key]['#type'] = 'textarea';
    }
  }

  $form['service'] = array(
    '#value'          => $service,
    '#type'           => 'value',
  );
  $form['service_name'] = array(
    '#value'          => $service['#method'],
    '#type'           => 'hidden',
  );

  $form['submit'] = array(
    '#ajax' => array(
      'callback'      => '_services_test_js',
      'wrapper'   => 'services-test-result',
      'progress'  => array('type' => 'bar', 'message' => t('Please wait...')),
    ),
    '#suffix'         => '<div id="services-test-result"></div>',
    '#type'           => 'submit',
    '#value'          => t('Test service')
  );
  return $form;
}

/**
 * AJAX callback to return a service test.
 *
 * @return
 *   Prints the services result in JSON format.
 */
function _services_test_js($form,$form_state) {
  $service  = $_POST['service_name'];
  $args     = $_POST['arg'];
  $result = services_method_call($service, $args);
  return drupal_to_js($result);
}
